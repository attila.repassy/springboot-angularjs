Integrate build process with maven and nodejs.

usage:
(prerequisites: git, java8 and maven3 installed)
* clone the project
* run 'mvn clean install' in the top level of the project (it takes a while at first, because the frontend-maven-plugin will download the node binaries and yarn)
* run 'mvn spring-boot:run' in the backend folder, wait for the application to start then open the app at http://localhost:8080.